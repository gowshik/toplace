<?php /* Template Name: Select Building Template  */ ?>

<?php get_header(); ?>
<div class="project_header text-center">
  <h1>SELECT A BUILDING:</h1>
</div>

<div class="d-flex flex-fill">
  <div class="container">
    <div class="row h-100">

      <div class="column column-3">
        <ul class="building-list">
          <li>
            <a href="<?php echo esc_url( home_url( '/select-level' ) );?>">
              Building 1
            </a>
          </li>
          <li>
            <a href="">
              Building 2
            </a>
          </li>
          <li>
            <a href="">
              Building 3
            </a>
          </li>
          <li>
            <a href="">
              Building 4
            </a>
          </li>
          <li>
            <a href="">
              Building 5
            </a>
          </li>
        </ul>
      </div>

      <div class="column column-10 buildings">
        <a href="<?php echo esc_url( home_url( '/select-level' ) );?>">
          <div class="building-image">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/building-1.svg;?>">
            <div class="building-name">
              <h4>Building 1:<br><span>Phoenix</span></h4>
            </div>
          </div>
        </a>

        <a href="">
          <div class="building-image">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/building-1.svg;?>">
            <div class="building-name">
              <h4>Building 2:<br><span>Hydra</span></h4>
            </div>
          </div>
        </a>

        <a href="">
          <div class="building-image">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/building-1.svg;?>">
            <div class="building-name">
              <h4>Building 3:<br><span>Vela</span></h4>
            </div>
          </div>
        </a>
        
      </div>
    </div>
  </div>
</div>