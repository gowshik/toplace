<?php /* Template Name: Select Level Template  */ ?>

<?php get_header(); ?>

<div class="project_header text-center">
  <h1>SELECT A LEVEL:</h1>
</div>

<div class="d-flex flex-fill">
  <div class="container">
    <div class="row h-100">
      <div class="column column-2">
        <ul class="level-list">
          <li>
            <a href=""><h6>Level 1</h6></a>
          </li>
          <?php for($i=2;$i<=11;$i++) {?>
          <li>
            <a href=""><h6>Level <?php echo $i; ?></h6></a>
          </li>
          <?php } ?>
        </ul>
      </div>
      <div class="column column-8 diagram">
        <ul class="diagram-level">
          <?php for($j=11;$j>=1;$j--) {?>
          <li>
            <a href="">
              <div class="line"></div>
              <h6>Level <?php echo $j; ?></h6>
            </a>
          </li>
          <?php } ?>
          <li>
            <a href="">
              <div class="line"></div>
              <h6>Ground Floor</h6>
            </a>
          </li>
          <li>
            <a href="">
              <div class="line"></div>
              <h6>Basement Level 1</h6>
            </a>
          </li>
          <li>
            <a href="">
              <div class="line"></div>
              <h6>Basement Level 2</h6>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>