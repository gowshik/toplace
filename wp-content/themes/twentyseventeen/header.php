
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">
	<?php wp_head(); ?>
	<link href="<?php echo get_template_directory_uri();?>/assets/css/style.css" rel="stylesheet">
</head>
<body <?php body_class(); ?>>
<?php if(is_page('homepage')) { ?>
<header id="masthead" class="site-header" role="banner">
		<div class="row justify-content-center">
			<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img class="header-logo" src="<?php echo get_template_directory_uri();?>/assets/images/logo.png">
			</a>
		</div>
</header>
<?php } else { ?>
<header id="masthead" class="site-header" role="banner">
	<div class="text-center alt-header">
		<div class="nav-buttons">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon-home.svg"></a>
			<button onclick="goBack()"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon-back.svg"></button>
		</div>
		<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img class="header-logo" src="<?php echo get_template_directory_uri();?>/assets/images/skyview-castlehill_colour-logo.png">
		</a>
		<div></div>
	</div>
</header>
<?php }?>

<script type="text/javascript">
	function goBack() {
    window.history.back();
	}
</script>