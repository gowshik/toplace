<?php /* Template Name: Menu page Template  */ ?>

<?php get_header(); ?>
<div class="d-flex flex-fill">
  <div class="container project-menu project-tiles">
    <div class="row">
      <div class="column">
        <a href="<?php echo esc_url( home_url( '/select-a-building' ) ); ?>">
          <div class="project-bg" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/images/skyview-bg-image.jpg);"></div>
          <div class="project-info">
            <h3>Floor Plans</h3>
          </div>
        </a>
      </div>
      <div class="column">
        <a href="">
          <div class="project-bg" style="background-image: url();"></div>
          <div class="project-info">
            <h3>Brochure</h3>
          </div>
        </a>
      </div>
      <div class="column">
        <a href="">
          <div class="project-bg" style="background-image: url();"></div>
          <div class="project-info">
            <h3>CGI's</h3>
          </div>
        </a>
      </div>
      <div class="column">
        <a href="">
          <div class="project-bg" style="background-image: url();"></div>
          <div class="project-info">
            <h3>Depreciation</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="row">
      <div class="column">
        <a href="">
          <div class="project-bg" style="background-image: url();"></div>
          <div class="project-info">
            <h3>Strata Levies</h3>
          </div>
        </a>
      </div>
      <div class="column">
        <a href="">
          <div class="project-bg" style="background-image: url();"></div>
          <div class="project-info">
            <h3>View Shots</h3>
          </div>
        </a>
      </div>
      <div class="column">
        <a href="">
          <div class="project-bg" style="background-image: url();"></div>
          <div class="project-info">
            <h3>Urbis Report</h3>
          </div>
        </a>
      </div>
      <div class="column">
        <a href="">
          <div class="project-bg" style="background-image: url();"></div>
          <div class="project-info">
            <h3>Executive Summary</h3>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
