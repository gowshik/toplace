<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'top_place');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{s)-Fbyw!eSKV[}]i87`NfwuqgK8;ues2:L;76 HuAq#w4O>Jr0o<qN.5}~,y{ZL');
define('SECURE_AUTH_KEY',  'n>1312@+bbu$9FX*hk6+_vnxAn$/19y^6N`D|K}`gDE(h]f9t+.Cj+*>y29[D?xn');
define('LOGGED_IN_KEY',    ':*!E}/s;ZG9&zOmNbTc)INlo+S4o9+qaV+3{ef0XzECL5!H_YK],*@.a13KO7NkT');
define('NONCE_KEY',        '(2i~OL:BT(hd{~X*S{@GL7k+@X1FOnuo-eX3)#t+@^Tb3g`Bi{M}a^{ltyhpr_|k');
define('AUTH_SALT',        '?*%W_Q{MqE/(|ksAQEbX#*=9*r2@pvWU=TGZ|p@A5o%+sl)-X_2OYS$%#!,dtYwb');
define('SECURE_AUTH_SALT', 'OYW[;VUnG[T{6QZp4]F(#{u$1^~YLlCZ;KBgrf$/$VC9CA|fR5~igP$/ypGeP<Pb');
define('LOGGED_IN_SALT',   '2}jeJI,[urCV<;90WUx)1YUZCnVRa3i;Vxa3?Q92AmSs5zS_QJ!s*]U9U!Ga*FL>');
define('NONCE_SALT',       'p+ |:6h{G,cpPk=gYxt;^6Y`rTL-#}<lh[Xd^tc44>&B6Vzq_Si8{j(1L2GK_Ga?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
