<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bodi
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bodi' ); ?></a>

	<header id="masthead" class="site-header">

			<div class="site-branding">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-3 text-center text-md-left mb-5 mb-md-0">
							<?php
							the_custom_logo();
							?>
							<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
								<?php
							$bodi_description = get_bloginfo( 'description', 'display' );
							if ( $bodi_description || is_customize_preview() ) :
								?>
								<p class="site-description"><?php echo $bodi_description; /* WPCS: xss ok. */ ?></p>
							<?php endif; ?>
						</div><!-- .col-sm-4 -->

						<div class="col-sm-12 col-md-9 branding-right text-center text-md-right">
							<a class="d-inline-block mb-3 mb-sm-0" href="<?php echo home_url('/login') ?>">Application login</a>
							<a class="d-inline-block" href="#enquire">Enquire Now</a>
						</div>
					</div><!-- .row -->
				</div>

				<div class="container d-xl-none" style="max-width: 100%;">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-5 order-2 order-md-1 branding-right text-center">
							<a class="d-inline-block mb-3 mb-sm-0" href="#">Application login</a>
						</div>

						<div class="col-12 col-md-2 order-1 order-md-2 mb-4 mb-md-0 text-center">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link" rel="home" itemprop="url">
								<img src="<?php echo get_template_directory_uri() . '/assets/images/logo-small.png' ?>" class="custom-logo" alt="Bodi Corporate" itemprop="logo">
							</a>
						</div><!-- .col-sm-4 -->

						<div class="col-12 col-sm-6 col-md-5 order-3 order-md-3 branding-right text-center">
							<a class="d-inline-block" href="#">Enquire Now</a>
						</div>
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .site-branding -->


		<!-- Each page banner -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
