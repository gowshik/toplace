<?php /* Template Name: Homepage Template  */ ?>

<?php get_header(); ?>
<div class="homepage_header text-center">
  <h1>CHOOSE YOUR PROJECT:</h1>
</div>
<div class="d-flex flex-fill">
  <div class="container">
    <div class="row project-tiles h-100">
      <div class="column">
        <a href="<?php echo esc_url( home_url( '/project1-menu-page' ) ); ?>">
          <div class="project-bg" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/images/skyview-bg-image.jpg);"></div>
          <div class="project-info">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/skyview-castlehill_logo.png">
            <div class="line"></div>
            <h2>Skyview Castle Hill</h2>
          </div>
        </a>
      </div>

      <div class="column">
        <a href="<?php echo esc_url( home_url( '/project1-menu-page' ) ); ?>">
          <div class="project-bg" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/images/grand-bg-image.jpg);"></div>
          <div class="project-info">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/grand-kellyville_logo.png">
            <div class="line"></div>
            <h2>Grand Kellyville</h2>
          </div>
        </a>
      </div>


    </div>
  </div>
</div>
